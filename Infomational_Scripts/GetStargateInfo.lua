--@name Get Stargate info
--@author JoshTheEnder

--[[
	This is just a small little script I made to see who was dialing who
	in the event that someone was constantly dialing someone elses gate and blocking it.
]]--
local SGLib = loadLibrary("stargate")
local EntLib = loadLibrary("ents")

StrGts = SGLib.getAllStargates() -- prints a list of all gates on the map to the console
for i = 1, #StrGts do
    printConsole(string.format("%i: Address: %s, Group: %s, Name: %s, Owner: %s", i, SGLib.address(StrGts[i]), SGLib.group(StrGts[i]), SGLib.name(StrGts[i]), StrGts[i]:owner() ) )    
end

function TellMeGateStuff(Gate) -- gets called when any stargate recieves an inbound wormhole
    local Targate = Gate:target() -- i'm so good with names :)
    printColor("[SG Info] ", {r=255,g=0,b=255}, string.format("Outbound conneciton from %s%s %q Owned by [%s] to %s%s %q Owned by [%s]", Targate:address(),Targate:group(),Targate:name(), Targate:owner(), Gate:address(),Gate:group(),Gate:name(), Gate:owner() ) )
end
SGLib.listen(TellMeGateStuff)
