--@name Gate Hologram
--@author JoshTheEnder
--@sharedscreen

--[[
       _           _  _______ _          ______           _           _        _____       _         _    _       _                                 
      | |         | ||__   __| |        |  ____|         | |         ( )      / ____|     | |       | |  | |     | |                                
      | | ___  ___| |__ | |  | |__   ___| |__   _ __   __| | ___ _ __|/ ___  | |  __  __ _| |_ ___  | |__| | ___ | | ___   __ _ _ __ __ _ _ __ ___  
  _   | |/ _ \/ __| '_ \| |  | '_ \ / _ \  __| | '_ \ / _` |/ _ \ '__| / __| | | |_ |/ _` | __/ _ \ |  __  |/ _ \| |/ _ \ / _` | '__/ _` | '_ ` _ \ 
 | |__| | (_) \__ \ | | | |  | | | |  __/ |____| | | | (_| |  __/ |    \__ \ | |__| | (_| | ||  __/ | |  | | (_) | | (_) | (_| | | | (_| | | | | | |
 _\____/ \___/|___/_| |_|_|  |_| |_|\___|______|_| |_|\__,_|\___|_|    |___/  \_____|\__,_|\__\___| |_|  |_|\___/|_|\___/ \__, |_|  \__,_|_| |_| |_|
 \ \    / /          (_)             |__ \                                                                                 __/ |                    
  \ \  / /__ _ __ ___ _  ___  _ __      ) |                                                                               |___/                     
   \ \/ / _ \ '__/ __| |/ _ \| '_ \    / /                                                                                                          
    \  /  __/ |  \__ \ | (_) | | | |  / /_                                                                                                          
     \/ \___|_|  |___/_|\___/|_| |_| |____|                                                                                                         
                                                                                                                                                    
                                                                                                                                                    
]]
local HoloLib = loadLibrary("holograms")
local EntLib = loadLibrary("ents")
local NetLib = loadLibrary("net")
local FindLib = loadLibrary("find")
local Chip = EntLib.self()

if SERVER then
    local WireLib = loadLibrary("wire")
    local SGLib = loadLibrary("stargate")
    local TimeLib = loadLibrary("time")
    local Chip = EntLib.self()
    local t = {}
    local HoloChev = {}
    local ScaleAmmount = 0.15

    function IsValid( Ent ) return Ent and Ent.isValid and Ent:isValid() end

    function clearTheHolos()
        for Holo in pairs( HoloLib.getAll().holos ) do Holo:remove() end
    end
    

    function connectTheGate() --[[ Set it up as a function so that we can call it later in the event the gate we're using ceases to exist ]]
        local pos = Chip:pos()
        local G = SGLib.getAllStargates()
        local D = math.huge
        for i=1, #G do
            d = G[i]:pos():DistToSqr(pos)
            if d < D and G[i]:owner() == EntLib.owner() then
                Gate = G[i]
                GateW = Gate:getWirelink()
                D = d
            end
        end
    end

    --[[ START OF ATLANTIS FUNCTION ]]
    function createAtlantisGate()
        t.gateType = "Atl"
        ChipAng = Chip:ang()
        ChipAng:RotateAroundAxis( Chip:right(), 90 )
        GateHolo = HoloLib.create(Chip:toWorld( Vector(0,0,6) ), ChipAng, "models/madman07/stargate/base_atlantis.mdl",  ScaleAmmount )
        GateHolo:setParent( Chip )
        EHHolo = HoloLib.create(GateHolo:toWorld( Vector(0,0,-0.07) ), GateHolo:ang(), "models/zup/stargate/stargate_horizon.mdl", ScaleAmmount )
        EHHolo:setParent( GateHolo )

        local ChevronAngles = { 40, 80, 120, 240, 280, 320, 0, 160, 200}
        for i = 1, 9 do
            local AngMult = ChevronAngles[ i ]
            HoloChev[i] = HoloLib.create( GateHolo:pos(), GateHolo:toWorld( Angle(0,0, - AngMult) ), "models/madman07/stargate/chevron.mdl", 0.15 )
            HoloChev[i]:setParent( GateHolo )
        end

        hook("think", "holo", function()
            if IsValid( Gate ) then
                Chevrons = Gate:chevrons()
                for i=1, #Chevrons do
                    if Chevrons:sub(i,i) == "1" then
                        HoloChev[i]:setSkin( 3 )
                    elseif Chevrons:sub(i,i) == "0" then
                        HoloChev[i]:setSkin( 2 )
                    end
                end
                if Gate:open() then
                    EHHolo:setColor(255, 255, 255, 255)
                elseif not Gate:open() then
                    EHHolo:setColor(255, 255, 255, 0)
                end
            end
        end)
    end
    --[[ END OF ATLANTIS FUNCTION ]]

    --[[ START OF MILKYWAY FUNCTION ]]
    function createMilkyWayGate(TypeID)
        t.gateType = ""
        ChipAng = Chip:ang()
        ChipAng:RotateAroundAxis( Chip:right(), 90 )
        GateHolo = HoloLib.create(Chip:toWorld( Vector(0,0,6) ), ChipAng, "models/madman07/stargate/base.mdl",  math.sqrt( ScaleAmmount ) )
        GateHolo:setParent( Chip )
        EHHolo = HoloLib.create(GateHolo:toWorld( Vector(0,0,-0.07) ), GateHolo:ang(), "models/zup/stargate/stargate_horizon.mdl", ScaleAmmount )
        EHHolo:setParent( GateHolo )
        RingHolo = HoloLib.create(GateHolo:pos(), GateHolo:ang(), "models/madman07/stargate/ring_sg1.mdl", math.sqrt( ScaleAmmount ) )
        RingHolo:setParent( GateHolo )

        local ChevronAngles = { 40, 80, 120, 240, 280, 320, 0, 160, 200}
        for i = 1, 9 do
            local AngMult = ChevronAngles[ i ]
            if TypeID == 1 and i == 7 then
                HoloChev[i] = HoloLib.create( GateHolo:pos(), GateHolo:toWorld( Angle(0,0, - AngMult) ), "models/madman07/stargate/chevron_movie.mdl", 0.15 )
            else
                HoloChev[i] = HoloLib.create( GateHolo:pos(), GateHolo:toWorld( Angle(0,0, - AngMult) ), "models/madman07/stargate/chevron.mdl", 0.15 )
            end
            HoloChev[i]:setParent( GateHolo )
        end

        hook("think", "holo", function()
            if IsValid( Gate ) then
                Chevrons = Gate:chevrons()
                for i=1, #Chevrons do
                    if Chevrons:sub(i,i) == "1" then
                        HoloChev[i]:setSkin( 1 )
                    elseif Chevrons:sub(i,i) == "0" then
                        HoloChev[i]:setSkin( 0 )
                    end
                end
                local rAng = Gate:gateRing():ang()
                RingHolo:setAng( GateHolo:toWorld( Angle(0,0,rAng.r) ) )
                if Gate:open() then
                    EHHolo:setColor(255, 255, 255, 255)
                    if TypeID == 1 and not chevLockDone then
                        for i=1, 9 do
                            HoloChev[i]:setAnim( HoloChev[i]:lookupSequence( "lock" ), 0, 1 )
                        end
                        chevLockDone = true
                    end
                elseif not Gate:open() then
                    EHHolo:setColor(255, 255, 255, 0)
                    if TypeID == 1 and chevLockDone then
                        for i=1, 9 do
                            HoloChev[i]:setAnim( HoloChev[i]:lookupSequence( "unlock" ), 0, 1 )
                        end
                        chevLockDone = false
                    end
                end
            end
        end)
    end
    --[[ END OF MILKYWAY FUNCTION ]]

    --[[  NETSEND THINK FUNCTION  ]]
    function NetworkingSend()
        if IsValid( Gate ) then
            if Gate:active() then
                if Gate:inbound() then
                    if Gate:open() and Gate:target() ~= nil then
                        t.gateStat = "Inbound\nWormhole"
                    elseif Gate:open() and Gate:target() == nil then
                        t.gateStat = "Wormhole\nDisengaging"
                    elseif not Gate:open() then
                        t.gateStat = "Inbound\nDialing"
                    end
                else
                    if Gate:open() and Gate:target() ~= nil then
                        t.gateStat = "Outbound\nWormhole"
                    elseif Gate:open() and Gate:target() == nil then
                        t.gateStat = "Wormhole\nDisengaging"
                    elseif not Gate:open() then
                        t.gateStat = "Outbound\nDialing"
                    end
                end
            else
                t.gateStat = "Gate Inactive"
            end
            if Gate:target() ~= nil then
                t.targAddr = Gate:target():address()
                t.targGrup = Gate:target():group()
                t.targName = Gate:target():name()
                if Gate:resolveAddress( Gate:target() ):len() < 9 then
                    t.addrLen = Gate:resolveAddress( Gate:target() ):len() + 1
                else
                    t.addrLen = Gate:resolveAddress( Gate:target() ):len()
                end
            else
                t.targAddr = ""
                t.targName = ""
                t.targGrup = ""
                t.addrLen = 0
            end
            t.nextChev = GateW['Dialing Symbol']
            t.gateAddr = Gate:address()
            t.gateName = Gate:name()
            t.gateActi = Gate:active()
            t.gateOpen = Gate:open()
            t.gateInbo = Gate:inbound()
            t.gateChev = GateW['Dialing Address']
            if Iris ~= nil then
                if SGLib.irisActive(Iris) then
                    t.irisActi = "closed"
                else
                    t.irisActi = "open"
                end
            else
                t.irisActi = "nil"
            end
            if NetLib.start() then
                NetLib.writeString( "HUDInfoSend" )
                NetLib.writeTable( t )
                NetLib.broadcast()
            end
        else
            t.gateStat = "GATE\nDISCONNECTED"
            t.gateChev = ""
            t.nextChev = ""
            if NetLib.start() then
                NetLib.writeString( "HUDInfoSend" )
                NetLib.writeTable( t )
                NetLib.broadcast()
            end
        end
    end
    --[[ END OF NETSEND FUNCTION  ]]

    hook("think", "NetSend", NetworkingSend)
    hook("think", "LetsGetThisStarted", function()
        if not IsValid( Gate ) then
            hook("think", "holo", nil)
            clearTheHolos()
            completed = false
            connectTheGate()
        elseif IsValid( Gate ) then
            if completed == false then
                print( string.format("Connected to %s%s ( %s )", Gate:address(), Gate:group(), Gate:name()) )
                if Gate:class() == "stargate_atlantis" then
                    createAtlantisGate()
                    completed = true
                elseif Gate:class() == "stargate_sg1" then
                    createMilkyWayGate(0)
                    completed = true
                elseif Gate:class() == "stargate_movie" then
                    createMilkyWayGate(1)
                    completed = true
                else
                    error( string.format( "Sorry, Gate Class %s is currently not supported, please use a supported gate", Gate:class() ) )
                end
            end
        end
    end)



--[[ ########## ALL BELOW THIS SECTION NEEDS TO BE RE-WRITTEN ##########

    
    if Gate:class() == "stargate_atlantis" then
        createAtlantisGate()
    else
        if Gate:class() == "stargate_sg1" then
            chevInactive = 0
            GateModel = "models/madman07/stargate/base.mdl"
            GateHolo = HoloLib.create(Vector(0,0,0), Angle(0,0,0), GateModel, math.sqrt( ScaleAmmount ) )
            chevActive = 1
            SpecialChev = "models/madman07/stargate/chevron.mdl"
            RingHolo = HoloLib.create(Vector(0,0,0), Angle(0,0,0), "models/madman07/stargate/ring_sg1.mdl", math.sqrt( ScaleAmmount ) )
            GateType = 1
        elseif Gate:class() == "stargate_movie" then
            chevInactive = 0
            GateModel = "models/madman07/stargate/base.mdl"
            GateHolo = HoloLib.create(Vector(0,0,0), Angle(0,0,0), GateModel, math.sqrt( ScaleAmmount ) )
            chevActive = 1
            SpecialChev = "models/madman07/stargate/chevron_movie.mdl"
            RingHolo = HoloLib.create(Vector(0,0,0), Angle(0,0,0), "models/madman07/stargate/ring_sg1.mdl", math.sqrt( ScaleAmmount ) )
            GateType = 1
        elseif Gate:class() == "stargate_universe" then
            GateModel = "models/the_sniper_9/universe/stargate/universegate.mdl"
            SGChevrons = HoloLib.create(Vector(0,0,0), Angle(0,0,0), "models/the_sniper_9/universe/stargate/universechevrons.mdl", ScaleAmmount )
            GateHolo = HoloLib.create(Vector(0,0,0), Angle(0,0,0), GateModel, ScaleAmmount )
            GateType = 2
        end
    end

    if Gate == nil then
        error("Couldn't find a gate you own!") -- shouldn't need this after the re-code
    end
    local GateRing = Gate:gateRing()
    IsIrised = false

    local Iris = Gate:iris()
    if Iris == nil then
        error( "You need to spawn an iris on your gate!" )
    end
    IrisModel = Iris:model()
    --  models/madman07/stargate/base_movie.mdl models/alexalx/stargate_cebt/sgtbase.mdl models/madman07/stargate/base_atlantis.mdl

    IrisHolo = HoloLib.create(Vector(0,0,0), Angle(0,0,0), IrisModel, 0.15 )
    EHHolo = HoloLib.create(Vector(0,0,0), Angle(0,0,0), "models/zup/stargate/stargate_horizon.mdl", ScaleAmmount )
    if GateType == 2 then
        EHHolo:setMaterial( "sgu/effect_02.vmt" )
    end

    if GateType == 1 then
        HoloChev[1] = HoloLib.create(Chip:pos(), Angle(0,0,0), "models/madman07/stargate/chevron.mdl", 0.15)
        HoloChev[2] = HoloLib.create(Chip:pos(), Angle(0,0,0), "models/madman07/stargate/chevron.mdl", 0.15)
        HoloChev[3] = HoloLib.create(Chip:pos(), Angle(0,0,0), "models/madman07/stargate/chevron.mdl", 0.15)
        HoloChev[4] = HoloLib.create(Chip:pos(), Angle(0,0,0), "models/madman07/stargate/chevron.mdl", 0.15)
        HoloChev[5] = HoloLib.create(Chip:pos(), Angle(0,0,0), "models/madman07/stargate/chevron.mdl", 0.15)
        HoloChev[6] = HoloLib.create(Chip:pos(), Angle(0,0,0), "models/madman07/stargate/chevron.mdl", 0.15)
        HoloChev[7] = HoloLib.create(Chip:pos(), Angle(0,0,0), SpecialChev, 0.15)
        HoloChev[8] = HoloLib.create(Chip:pos(), Angle(0,0,0), "models/madman07/stargate/chevron.mdl", 0.15)
        HoloChev[9] = HoloLib.create(Chip:pos(), Angle(0,0,0), "models/madman07/stargate/chevron.mdl", 0.15)
    end
    -- skin 0 = SG1 inactive, 1 = SG1 active, 2 = ATL inactive, 3 = ATL Inactive, 4 = Infinity inactive, 5 = infinity active
    -- chev animation, lock = chevron lock, unlock = chevron unlock

    IrisHolo:setMaterial("materials/zup/stargate/shield_at")

    if RingHolo ~= nil then
        RingHolo:setPos( GateHolo:toWorld( Vector(0,0,0) ) )
        RingHolo:setAng( GateHolo:toWorld( Angle(0,0,0) ) )
        RingHolo:setParent( GateHolo )
    end


    hook("think", "holo", function()
        ChipAng = Chip:ang()
        ChipAng:RotateAroundAxis( Chip:right(), 90 )
        if RingHolo ~= nil then
            local rAng = GateRing:ang()
            RingHolo:setAng( GateHolo:toWorld( Angle(0,0,rAng.r) ) )
        end
        if GateType == 2 then
            local rAng = GateRing:ang()
            SGChevrons:setAng( ChipAng + Angle(0,0,rAng.r) )
            GateHolo:setAng( ChipAng + Angle(0,0, rAng.r) )
        end

        Chevrons = Gate:chevrons()

        for i=1, #Chevrons do
            if Chevrons:sub(i,i) == "1" then
                HoloChev[i]:setSkin( chevActive )
            elseif Chevrons:sub(i,i) == "0" then
                HoloChev[i]:setSkin( chevInactive )
            end
        end
        --[[
        if Gate:active() then
            Chip:setColor(255,255,255,0)
            if GateType == 2 then
                SGChevrons:setMaterial( "The_Sniper_9/Universe/Stargate/UniverseChevronOn.vmt" )
            end
        else
            Chip:setColor(255,255,255,255)
            if GateType == 2 then
                SGChevrons:setMaterial( "The_Sniper_9/Universe/Stargate/UniverseChevronOff.vmt" )
            end
        end

        if Gate:open() then
            EHHolo:setColor(255, 255, 255, 255)
        elseif not Gate:open() then
            EHHolo:setColor(255, 255, 255, 0)
        end
        --[[ IRIS SECTION 
        if IrisModel == "models/zup/stargate/sga_shield.mdl" then
            if SGLib.irisActive( Iris ) then
                IrisHolo:setColor(255,255,255, 255)
            elseif not SGLib.irisActive( Iris ) then
                IrisHolo:setColor(255,255,255, 0)
            end
        elseif IrisModel == "models/madman07/shields/goauld_iris.mdl" then
            IrisHolo:setMaterial( "Models/effects/comball_sphere" )
            if SGLib.irisActive( Iris ) then
                IrisHolo:setColor(255,255,255, 255)
            elseif not SGLib.irisActive( Iris ) then
                IrisHolo:setColor(255,255,255, 0)
            end
        else
            if SGLib.irisActive( Iris ) == true and IsIrised == false then
                IrisHolo:setAnim( IrisHolo:lookupSequence( "iris_close" ), 0, 1 )
                IsIrised = true
            elseif SGLib.irisActive( Iris ) == false and IsIrised == true then
                IrisHolo:setAnim( IrisHolo:lookupSequence( "iris_open" ), 0, 1 )
                IsIrised = false
            end
        end
        --[[ END OF IRIS SECTION  
    end)

    ]]-- end of massive comment block
end

if CLIENT then
    local RenderLib = loadLibrary( "render" )
    local Table = {}
    Table.gateChev = "123456789"
    function NetStuff()
        if NetLib.readString() == "HUDInfoSend" then
            Table = NetLib.readTable()
        end
    end

    local Font = RenderLib.createFont( string.format("Stargate Address Glyphs Sg1", Table.gateType), 32, 560, true)
    local Font2 = RenderLib.createFont( "DermaLarge ", 26, 750, true )
    local Font3 = RenderLib.createFont( "DermaLarge ", 26.5, 750, true, true, true )
    --local Font = RenderLib.createFont( "DermaLarge", 26, 750, true )
    --(font, size, weight, antialias, additive, shadow, outline, blur)
    hook("net", "networking", NetStuff )


    hook( "render", "Stargate3D2D", function()
        local Angles = Chip:ang()
        Angles:RotateAroundAxis( Chip:right(), 180 )
        Angles:RotateAroundAxis( Chip:forward(), 180 )
        Angles:RotateAroundAxis( Chip:up(), -90 )

        RenderLib.start3D2D( Chip:toWorld( Vector(-40,-40,6) ), Angles, 0.5 )
            RenderLib.setColor(0,0,0,100)
            RenderLib.drawRect(0, 0, 160, 160)
            RenderLib.setColor(0,0,0,255)
            RenderLib.drawText( Font3, 80, -20, string.format("%s", Table.gateStat), RenderLib.TEXT_ALIGN_CENTER )
            RenderLib.setColor(255,0,0,255)
            RenderLib.drawText( Font2, 80, -20, string.format("%s", Table.gateStat), RenderLib.TEXT_ALIGN_CENTER )
            --RenderLib.drawText( Font2, 80, -50, string.format("%s", Table.addrLen), RenderLib.TEXT_ALIGN_CENTER )
            RenderLib.setColor(0,0,255,255)
            --RenderLib.drawText( Font, 80, 130, string.format("%s", Table.gateChev), RenderLib.TEXT_ALIGN_CENTER )
        RenderLib.end3D2D()

        RenderLib.start3D2D( Chip:toWorld( Vector(-40,-40,7.5) ), Angles, 0.3 )
            RenderLib.setColor(0,255,0,255)
            RenderLib.drawText( Font, 130,120, string.format("%s", Table.nextChev), RenderLib.TEXT_ALIGN_CENTER )

            RenderLib.drawText( Font, 200, 50, Table.gateChev:sub(1,1), RenderLib.TEXT_ALIGN_CENTER )
            RenderLib.drawText( Font, 220, 100, Table.gateChev:sub(2,2), RenderLib.TEXT_ALIGN_CENTER )
            RenderLib.drawText( Font, 210, 160, Table.gateChev:sub(3,3), RenderLib.TEXT_ALIGN_CENTER )

            RenderLib.drawText( Font, 70, 50, Table.gateChev:sub(6,6), RenderLib.TEXT_ALIGN_CENTER )
            RenderLib.drawText( Font, 50, 100, Table.gateChev:sub(5,5), RenderLib.TEXT_ALIGN_CENTER )
            RenderLib.drawText( Font, 60, 160, Table.gateChev:sub(4,4), RenderLib.TEXT_ALIGN_CENTER )

            if Table.addrLen == 7 then
                RenderLib.drawText( Font, 140, 35, Table.gateChev:sub(7,7), RenderLib.TEXT_ALIGN_CENTER )
            elseif Table.addrLen == 8 then
                RenderLib.drawText( Font, 140, 35, Table.gateChev:sub(8,8), RenderLib.TEXT_ALIGN_CENTER )
                RenderLib.drawText( Font, 165, 195, Table.gateChev:sub(7,7), RenderLib.TEXT_ALIGN_CENTER )
            elseif Table.addrLen == 9 then
                RenderLib.drawText( Font, 135, 35, Table.gateChev:sub(9,9), RenderLib.TEXT_ALIGN_CENTER )
                RenderLib.drawText( Font, 100, 195, Table.gateChev:sub(8,8), RenderLib.TEXT_ALIGN_CENTER )
                RenderLib.drawText( Font, 165, 195, Table.gateChev:sub(7,7), RenderLib.TEXT_ALIGN_CENTER )
            end
        RenderLib.end3D2D()
    end)
end
